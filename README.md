# README #

**Front-end test - CHAS**

* Show cards.
* Click card to put it face down.
* Click card again to reveal.
* Click Reset button to reset.

Colors and fonts inspired by Chas.se  

---

** Specification **

It shall have Chas (http://www.chas.se) look and feel and should look amazing on both a desktop, and a mobile browser. Zip all files thats needed for the application to work and name it with your first and your last name and send it back.

The planningDeck shall have 3 steps:

1. Select phase: You can select any card. When you select one, you will move on to the Waiting phase.
2. Waiting phase: Only show the chosen card with its backside up (awaiting others to
choose a card using some similar app or actual cards). An simple interaction shall get  you to Reveal phase.
3. Reveal phase: The card that you chose in step one is revealed. Another interaction will then get you to Select phase again.

---

** Execution **

*Languages, structure and planning*

This is written in HTML, CSS, JavaScript, jQuery.
I used the languages that I am familiar with to make the most out of the task as possible within the time frame. jQuery is easily manageable and widely spread, in case there will be any further development.

*Design*

I am by no means a designer even though I personally appreciate colors. I was inspired by Chas.se, the official website and wanted to follow these pointers that I observed:

* CHAS-colors: grey, blue (green, orange, red, purple)
* Laid back but sincere
* Distinct and easily interpreted

---

** Conclusion **

If I had more time, I would have read up on how to do this with React as base instead of jQuery. JavaScript overall is my weakest point.

I am not particularly proud of the design but I was aiming to make it look decent, safe for color blind people and have a feeling of Chas. I am clearly NOT a designer. ;) 

I would have also loved to add some animation to make it all feel and look more alive. For now I focused on delivering a stable product over a glitzy product. Enjoy!