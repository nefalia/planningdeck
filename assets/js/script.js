$(document).ready(function(){ //för snabbare laddtid än window load

    //samtliga kort
    var allcards = [ "cake", "1/2", "1", "2", "3", "5", "8", "16", "20", "40", "100", "?" ];

    // mata ut kort
    var showCards = "<ul class='card'>";
    for (var i = 0; i < allcards.length; i++) {                   //för varje kort som existerar läggs korten ut
      showCards += "<li class='cardface'>";                       //kortens framsida
      showCards += "<div class='cardup'>" + allcards[i] + "</div>" //kort, ansiktet synligt
      showCards += "</li>";
    }
    showCards += "</ul>";

    document.getElementById("container").innerHTML = showCards; //fäst korten i element med id "container"

    //välj ett kort
    $(".cardface").on('mousedown touchstart', function(e){      //när ett kort klickas på

      //för att få klick att fungera på mobila enheter samt undvika dubbelklick och felmeddelanden i IE
      event.preventDefault ? event.preventDefault() : (event.returnValue = false);

      if ($(this).children("div").hasClass("cardup")) {         //om kortet ligger med ansikte synligt

        $(this).children("div").removeClass("cardup");          //ta bort klass för att visa ansiktet
        $(this).children("div").addClass("carddown");           //lägg till klass för att lägga kortet med ansikte nedåt

        $(this).prevAll().hide();                               //alla andra kort försvinner
        $(this).nextAll().hide();

        $("ul.card").toggleClass("single");                     //det utvalda kortet hamnar i mitten

      } else if ($(this).children("div").hasClass("carddown")) { //annars om kortet ligger med ansikte nedåt

        $(this).children("div").removeClass("carddown");        //ta bort klass för ansikte nedåt
        $(this).children("div").addClass("cardup");             //lägg till klass för ansikte uppåt

        $("a.resetbtn").toggleClass("show");                    //visa omstartsknapp
      }
    });

    //starta om
    $("a.resetbtn").click(function(){

      location.reload(true);  //ladda om sidan helt, funkar därmed på mobil samt desktop

      // nedanstående fungerar enbart på desktop, därav utkommenterat
      // document.getElementById("container").innerHTML = showCards; //lägg ut alla kort igen
      // $("button.resetbtn").toggleClass("show");                   //dölj omstartsknapp
    });
});
